pipeline {
  agent any
  environment {
    DOCKER_HUB_USER = "crgv"
    DOCKER_HUB_PASSWORD = credentials('dockerhub_password') 
    TAG_VERSION = '1.2'
    IMAGE_NAME = 'cv_web'
    SONAR_TOKEN = credentials('sonar_token')
  }
  stages {
    stage('Build') {
      steps {
        sh './gradlew clean assemble'
      }
    }

    stage('Test') {
      steps {
        sh './gradlew test'
      }
      post {
        always {
          junit 'build/test-results/test/*.xml'
          archiveArtifacts 'build/reports/tests/test/**/*'
        }
      }
    }

    stage('Code Quality') {
      steps {
        sh './gradlew sonarqube'
      }
    }

    stage('Package'){
      steps {
        sh 'docker build -f ./Dockerfile.dev -t "${IMAGE_NAME}":"${TAG_VERSION}" .'
      }
    }

    stage('Publish'){
      steps {
        sh 'docker login -u "${DOCKER_HUB_USER}" -p "${DOCKER_HUB_PASSWORD}"'
        sh 'docker tag "${IMAGE_NAME}":"${TAG_VERSION}" crgv/"${IMAGE_NAME}":"${TAG_VERSION}"'
        sh 'docker push crgv/"${IMAGE_NAME}":"${TAG_VERSION}"'
      }
    }
  }
}